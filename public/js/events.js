window.addEventListener('created', event => {
    $("#createCategoryModal").modal('hide')
    $.notify('Created Successfully' , 'success')
})

window.addEventListener('updated', event => {
    $("#editCategoryModal").modal('hide')
    $.notify('Updated Successfully' , 'success')
})

window.addEventListener('deleted', event => {
    $("#deleteCategoryModal").modal('hide')
    $.notify('Deleted Successfully' , 'success')
})