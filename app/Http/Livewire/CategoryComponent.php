<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\DTO\CategoryData;
use App\Services\CategoryService;
use Livewire\Component;

class CategoryComponent extends Component
{
    public $categories, $title, $description, $category_id;

    public function render()
    {
        $categoryService = new CategoryService();
        $this->categories = $categoryService->list();
        return view('livewire.category-component')
            ->extends('layouts.app');
    }

    public function upsert()
    {
        if(auth()->check())
        {
            $categoryService = new CategoryService();
            $rules = CategoryData::rules();
            $messages = CategoryData::messages();
            $this->validate($rules,$messages);
            $data = CategoryData::from([
                'id' => $this->category_id,
                'title' => $this->title,
                'description' => $this->description
            ]);
            $categoryService->upsert($data);
            if($this->category_id)
                $this->dispatchBrowserEvent('updated');
            else
                $this->dispatchBrowserEvent('created');
            $this->resetInputs();
            
        }else{
            return route('login');
        }

        
    }

    public function edit(Category $category)
    {
        $this->category_id = $category->id;
        $this->title = $category->title;
        $this->description = $category->description;
    }

    public function deleteModal(Category $category)
    {
        $this->category_id = $category->id;
        $this->title = $category->title;
        $this->description = $category->description;
    }
   
    public function delete()
    {
        if(auth()->check())
        {
            $categoryService = new CategoryService();
            $categoryService->delete($this->category_id);
            $this->dispatchBrowserEvent('deleted');
        }else{
            return route('login');
        }
    }

    public function resetInputs()
    {
        $this->category_id = '';
        $this->title = '';
        $this->description = '';
    }
}
