<?php 
namespace App\Models\DTO;

use Spatie\LaravelData\Data;


class CategoryData extends Data {
    
    public function __construct(
        public ?int $id,
        public string $title,
        public string $description
    )
    {
        
    }

    public static function rules(): array
    {
        return [
            'title' => 'required|string',
            'description' => 'required'
        ];
    }

    public static function messages()
    {
        return [
            'title.required' => 'this title is required property',
            'title.string' => 'this title must be string value',
            'description.required' => 'this description is required value'
        ];
    } 
}