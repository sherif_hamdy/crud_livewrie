<?php 
namespace App\Actions\Category;

use App\Models\Category;

class DeleteCategoryAction {

    public static function execute($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
    }
}
