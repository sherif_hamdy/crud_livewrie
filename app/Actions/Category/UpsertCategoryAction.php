<?php 
namespace App\Actions\Category;

use App\Models\Category;
use App\Models\DTO\CategoryData;

class UpsertCategoryAction {

    public static function execute(CategoryData $data)
    {
        Category::updateOrCreate(
            [
                'id' => $data->id
            ],
            [
                ...$data->all()
            ]
        );
    }
}
