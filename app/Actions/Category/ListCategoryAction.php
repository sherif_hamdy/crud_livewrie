<?php 
namespace App\Actions\Category;

use App\Models\Category;

class ListCategoryAction {

    public static function execute()
    {
        return Category::all();
    }
}
