<?php 

namespace App\Services;

use App\Actions\Category\DeleteCategoryAction;
use App\Actions\Category\ListCategoryAction;
use App\Actions\Category\UpsertCategoryAction;
use App\Models\DTO\CategoryData;

class CategoryService {
    
    public function list()
    {
        return ListCategoryAction::execute();
    }

    public function upsert(CategoryData $data)
    {
        UpsertCategoryAction::execute($data);
    }

    public function delete($id)
    {
        DeleteCategoryAction::execute($id);
    }
}