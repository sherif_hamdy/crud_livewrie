<div>
    <div class="row">
        <div class="col-md-3 offset-2">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#createCategoryModal">
                Create Category 
            </button>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-8 offset-2">
            <table class="table table-bordered table-striped"> 
                <thead>
                    <th>#</th>
                    <th>Category Title</th>
                    <th>Category Description</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $category->title }}</td>
                            <td>{{ $category->description }}</td>
                            <td>
                                <button type="button" class="btn btn-primary" wire:click.prevent="edit({{ $category->id }})" data-bs-toggle="modal" data-bs-target="#editCategoryModal">
                                    Edit
                                </button>
                                <button type="button" class="btn btn-danger" wire:click.prevent="deleteModal({{ $category->id }})" data-bs-toggle="modal" data-bs-target="#deleteCategoryModal">
                                    delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('livewire.create-category')
    @include('livewire.edit-category')
    @include('livewire.delete-category')
</div>
