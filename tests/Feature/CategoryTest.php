<?php

namespace Tests\Feature;

use App\Http\Livewire\CategoryComponent;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\CreatesApplication;
use Tests\TestCase;
use App\Models\Category;
use Livewire\Livewire;

class CategoryTest extends TestCase
{
    use CreatesApplication, RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->withExceptionHandling();

        $this->actingAs(User::factory()->create());
        
        $component = Livewire::test(CategoryComponent::class);

        $component->assertViewis('livewire.category-component');
        $component->assertViewHas('categories', Category::all());
    }

    public function test_user_can_add_category()
    {
        $this->withExceptionHandling();

        $this->actingAs(User::factory()->create());

        $category = Category::factory()->make();

        Livewire::test(CategoryComponent::class)
                ->set('title', $category->title)
                ->set('description', $category->description)
                ->call('upsert');

        $this->assertCount(1, Category::all());
    }
    

    public function test_unauthenticated_user_cannot_create_category()
    {
        $this->withExceptionHandling();

        $category = Category::factory()->make();

        Livewire::test(CategoryComponent::class)
                ->set('title', $category->title)
                ->set('description', $category->description)
                ->call('upsert');

        $this->assertCount(0, Category::all());
    }

    public function test_title_is_required()
    {
        $this->withExceptionHandling();

        $this->actingAs(User::factory()->create());

        $category = Category::factory()->make();

        Livewire::test(CategoryComponent::class)
                ->set('title', '')
                ->set('description', $category->description)
                ->call('upsert')
                ->assertHasErrors(['title']);
    }

    public function test_description_is_required()
    {
        $this->withExceptionHandling();

        $this->actingAs(User::factory()->create());

        $category = Category::factory()->make();

        Livewire::test(CategoryComponent::class)
                ->set('title', $category->title)
                ->set('description', '')
                ->call('upsert')
                ->assertHasErrors(['description']);
    }

    public function test_user_can_update_category()
    {
        $this->withExceptionHandling();

        $this->actingAs(User::factory()->create());

        $category = Category::factory()->create();

        Livewire::test(CategoryComponent::class)
                ->set('category_id', $category->id)
                ->set('title', 'updated')
                ->set('description', 'description updated')
                ->call('upsert');

        $this->assertDatabaseHas('categories', ['id' => $category->id, 'title' => 'updated', 'description' => 'description updated']);
    }

    public function test_user_can_delete_category()
    {
        $this->withExceptionHandling();

        $this->actingAs(User::factory()->create());

        $category = Category::factory()->create();

        Livewire::test(CategoryComponent::class)
                ->set('category_id' , $category->id)
                ->call('delete');

        $this->assertCount(0, Category::all());
        $this->assertDatabaseMissing('categories', ['id' => $category->id]);
    }
}
